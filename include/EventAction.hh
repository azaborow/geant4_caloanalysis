//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: EventAction.hh 75215 2013-10-29 16:07:06Z gcosmo $
// 
/// \file EventAction.hh
/// \brief Definition of the EventAction class

#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4SystemOfUnits.hh"

/// Event action class
///

class EventAction : public G4UserEventAction
{
  public:
    EventAction();
    virtual ~EventAction();

    virtual void  BeginOfEventAction(const G4Event* event);
    virtual void    EndOfEventAction(const G4Event* event);
    
    void AddStep(G4double ekin);
    void AddEnergyDeposit(G4double edep, G4double ekin);
    
  private:
    G4double  fEnergy1;
    G4double  fEnergy10;
    G4double  fEnergy100;
    G4double  fStep1ev;
    G4double  fStep10ev;
    G4double  fStep100ev;
    G4double  fStep1kev;
    G4double  fStep10kev;
    G4double  fStep100kev;
    G4double  fStep1;
    G4double  fStep4;
    G4double  fStep20;
    G4double  fStep100;
    G4double  fStep500;
    G4double  fStep1000;
    G4double  fStepAll;
};

// inline functions

inline void EventAction::AddEnergyDeposit(G4double edep, G4double ekin) {
  if(ekin < 100*MeV) fEnergy100 += edep; 
  if(ekin < 10*MeV) fEnergy10 += edep; 
  if(ekin < 1*MeV) fEnergy1 += edep; 
}
inline void EventAction::AddStep(G4double ekin) {
  if(ekin < 20*MeV) fStep20 += 1; 
  if(ekin < 4*MeV) fStep4 += 1; 
  if(ekin < 1*MeV) fStep1 += 1; 
  if(ekin < 100*MeV) fStep100 += 1; 
  if(ekin < 500*MeV) fStep500 += 1; 
  if(ekin < 1*GeV) fStep1000 += 1;  
  if(ekin < 1*eV) fStep1ev += 1;  
  if(ekin < 10*eV) fStep10ev += 1;  
  if(ekin < 100*eV) fStep100ev += 1; 
  if(ekin < 1*keV) fStep1kev += 1;  
  if(ekin < 10*keV) fStep10kev += 1;  
  if(ekin < 100*keV) fStep100kev += 1; 
  fStepAll += 1; 
}
                     
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
