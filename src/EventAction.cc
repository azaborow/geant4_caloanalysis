//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: EventAction.cc 100946 2016-11-03 11:28:08Z gcosmo $
// 
/// \file EventAction.cc
/// \brief Implementation of the EventAction class

#include "EventAction.hh"
#include "RunAction.hh"
#include "Analysis.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4UnitsTable.hh"

#include "Randomize.hh"
#include <iomanip>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::EventAction()
 : G4UserEventAction(),
   fEnergy100(0.),
   fEnergy10(0.),
   fEnergy1(0.),
   fStep1000(0.),
   fStep500(0.),
   fStep100(0.),
   fStep20(0.),
   fStep4(0.),
   fStep1(0.),
   fStepAll(0),
   fStep1ev(0),
   fStep10ev(0),
   fStep100ev(0),
   fStep1kev(0),
   fStep10kev(0),
   fStep100kev(0)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EventAction::~EventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::BeginOfEventAction(const G4Event* /*event*/)
{  
  // initialisation per event
  fEnergy100 = 0.;
  fEnergy10 = 0.;
  fEnergy1 = 0.;
  fStep1000=0;
  fStep500=0;
  fStep100=0;
  fStep20=0;
  fStep4=0;
  fStep1=0.;
  fStepAll=0;
  fStep1ev=0;
  fStep10ev=0;
  fStep100ev=0;
  fStep1kev=0;
  fStep10kev=0;
  fStep100kev=0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void EventAction::EndOfEventAction(const G4Event* event)
{
  // Accumulate statistics
  //

  // get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

  // fill histograms
  analysisManager->FillH1(0, fEnergy1);
  analysisManager->FillH1(1, fEnergy10);
  analysisManager->FillH1(2, fEnergy100);

  analysisManager->FillH1(3, fStep1ev);
  analysisManager->FillH1(4, fStep10ev);
  analysisManager->FillH1(5, fStep100ev);
  analysisManager->FillH1(6, fStep1kev);
  analysisManager->FillH1(7, fStep10kev);
  analysisManager->FillH1(8, fStep100kev);
  analysisManager->FillH1(9, fStep1);
  analysisManager->FillH1(10, fStep4);
  analysisManager->FillH1(11, fStep20);
  analysisManager->FillH1(12, fStep100);
  analysisManager->FillH1(13, fStep500);
  analysisManager->FillH1(14, fStep1000);
  analysisManager->FillH1(15, fStepAll);
  
  // Print per event (modulo n)
  //
  auto eventID = event->GetEventID();
  auto printModulo = G4RunManager::GetRunManager()->GetPrintProgress();
  if ( ( printModulo > 0 ) && ( eventID % printModulo == 0 ) ) {
    G4cout << " Number of ALL steps: " << fStepAll << G4endl;
  }
}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
