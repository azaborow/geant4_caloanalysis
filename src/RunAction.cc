//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: RunAction.cc 100946 2016-11-03 11:28:08Z gcosmo $
//
/// \file RunAction.cc
/// \brief Implementation of the RunAction class

#include "RunAction.hh"
#include "Analysis.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::RunAction()
 : G4UserRunAction()
{ 
  // set printing event number per each event
  G4RunManager::GetRunManager()->SetPrintProgress(1);     

  // Create analysis manager
  // The choice of analysis technology is done via selectin of a namespace
  // in Analysis.hh
  auto analysisManager = G4AnalysisManager::Instance();
  G4cout << "Using " << analysisManager->GetType() << G4endl;

  // Create directories 
  //analysisManager->SetHistoDirectoryName("histograms");
  //analysisManager->SetNtupleDirectoryName("ntuple");
  analysisManager->SetVerboseLevel(1);
  analysisManager->SetNtupleMerging(true);
    // Note: merging ntuples is available only with Root output

  // Book histograms, ntuple
  //

  // Creating histograms
  analysisManager->CreateH1("E1MeV","Energy", 100, 0., 10000*MeV);
  analysisManager->CreateH1("E10MeV","Energy", 100, 0., 10000*MeV);
  analysisManager->CreateH1("E100MeV","Energy", 100, 0., 10000*MeV);

  analysisManager->CreateH1("stepsE1eV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE10eV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE100eV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE1keV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE10keV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE100keV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE1MeV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE4MeV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE20MeV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE100MeV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE500MeV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsE1000MeV","Steps", 1e6, 0., 1e6);
  analysisManager->CreateH1("stepsAll","Steps", 1e6, 0., 1e6);

}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{
  delete G4AnalysisManager::Instance();  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::BeginOfRunAction(const G4Run* /*run*/)
{ 
  //inform the runManager to save random number seed
  //G4RunManager::GetRunManager()->SetRandomNumberStore(true);
  
  // Get analysis manager
  auto analysisManager = G4AnalysisManager::Instance();

  // Open an output file
  //
  G4String fileName = "";
  analysisManager->OpenFile(fileName);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run* /*run*/)
{
  // print histogram statistics
  //
  auto analysisManager = G4AnalysisManager::Instance();
  if ( analysisManager->GetH1(1) ) {
    G4cout << G4endl << " ----> print histograms statistic ";
    if(isMaster) {
      G4cout << "for the entire run " << G4endl << G4endl; 
    }
    else {
      G4cout << "for the local thread " << G4endl << G4endl; 
    }

    G4cout << " Energy deposited by particles with E<1 MeV : mean = "
           << G4BestUnit(analysisManager->GetH1(0)->mean(),"Energy")
           << " rms = " << G4BestUnit(analysisManager->GetH1(0)->rms(),  "Energy") << G4endl;
    G4cout << " Energy deposited by particles with E<10 MeV : mean = "
           << G4BestUnit(analysisManager->GetH1(1)->mean(),"Energy")
           << " rms = " << G4BestUnit(analysisManager->GetH1(1)->rms(),  "Energy") << G4endl;
    G4cout << " Energy deposited by particles with E<100 MeV : mean = "
           << G4BestUnit(analysisManager->GetH1(2)->mean(),"Energy")
           << " rms = " << G4BestUnit(analysisManager->GetH1(2)->rms(),  "Energy") << G4endl;

    G4cout << " Number of simulation steps done by particles with E<1 eV : mean = " <<  analysisManager->GetH1(3)->mean()
           << " rms = " << analysisManager->GetH1(3)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<10 eV : mean = " <<  analysisManager->GetH1(4)->mean()
           << " rms = " << analysisManager->GetH1(4)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<100 eV : mean = " <<  analysisManager->GetH1(5)->mean()
           << " rms = " << analysisManager->GetH1(5)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<1 keV : mean = " <<  analysisManager->GetH1(6)->mean()
           << " rms = " << analysisManager->GetH1(6)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<10 keV : mean = " <<  analysisManager->GetH1(7)->mean()
           << " rms = " << analysisManager->GetH1(7)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<100 keV : mean = " <<  analysisManager->GetH1(8)->mean()
           << " rms = " << analysisManager->GetH1(8)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<1 MeV : mean = " <<  analysisManager->GetH1(9)->mean()
           << " rms = " << analysisManager->GetH1(9)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<4 MeV : mean = " <<  analysisManager->GetH1(10)->mean()
           << " rms = " << analysisManager->GetH1(10)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<20 MeV : mean = " <<  analysisManager->GetH1(11)->mean()
           << " rms = " << analysisManager->GetH1(11)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<100 MeV : mean = " <<  analysisManager->GetH1(12)->mean()
           << " rms = " << analysisManager->GetH1(12)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<500 MeV : mean = " <<  analysisManager->GetH1(13)->mean()
           << " rms = " << analysisManager->GetH1(13)->rms() << G4endl;
    G4cout << " Number of simulation steps done by particles with E<1 GeV : mean = " <<  analysisManager->GetH1(14)->mean()
           << " rms = " << analysisManager->GetH1(14)->rms() << G4endl;
    G4cout << " Number of simulation steps done by all the particles : mean = " <<  analysisManager->GetH1(15)->mean()
           << " rms = " << analysisManager->GetH1(15)->rms() << G4endl;

  }

  // save histograms & ntuple
  //
  analysisManager->Write();
  analysisManager->CloseFile();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
