//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id: SteppingAction.cc 100946 2016-11-03 11:28:08Z gcosmo $
// 
/// \file SteppingAction.cc
/// \brief Implementation of the SteppingAction class

#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"

#include "G4Step.hh"
#include "G4RunManager.hh"

#include "G4Electron.hh"
#include "G4Positron.hh"
#include "G4Gamma.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::SteppingAction(
                      EventAction* eventAction)
  : G4UserSteppingAction(),
    fEventAction(eventAction)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

SteppingAction::~SteppingAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::UserSteppingAction(const G4Step* step)
{
  // energy deposit
  auto edep = step->GetTotalEnergyDeposit();

  auto ekin = step->GetTrack()->GetKineticEnergy();

#ifdef CHECK_ONLY_EM
  // // check only the number of EM particles (it will not change results for EM showers, but it will influence hadronic showers)
  if(step->GetTrack()->GetDefinition() == G4Positron::Positron() || step->GetTrack()->GetDefinition() == G4Electron::Electron() || step->GetTrack()->GetDefinition() == G4Gamma::Gamma())
#endif
{
    fEventAction->AddStep(ekin);
    fEventAction->AddEnergyDeposit(edep,ekin);

#ifdef KILL_BELOW_THRESHOLD
    // // check the number of particles that have certain energy
    if (ekin < THRESHOLD_KILL) {
      step->GetTrack()->SetTrackStatus(fStopAndKill);
    }
#endif
}
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
