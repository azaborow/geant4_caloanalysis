#ifdef HAVE_HEPMC3
#include "GeneratorSettings.hh"


#include <G4UIcmdWithAString.hh>
#include <G4UIdirectory.hh>

#include <HepMC3/GenEvent.h>
#include <HepMC3/ReaderAscii.h>

GeneratorSettingsMessenger::GeneratorSettingsMessenger() {
  fDirectory.reset(new G4UIdirectory("/generator/"));
  fDirectory->SetGuidance("Set generator parameters.");

  fHepMCfile.reset(new G4UIcmdWithAString("/generator/hepMCfile", this));
  fHepMCfile->SetGuidance("Read event from HepMC3 file.");
}

GeneratorSettingsMessenger::~GeneratorSettingsMessenger() {
  for (const HepMC3::GenEvent *event : fSettings.events) {
    delete event;
  }
}

void GeneratorSettingsMessenger::SetNewValue(G4UIcommand *command,
                                             G4String newValue) {
  if (command == fHepMCfile.get()) {
    fSettings.events.clear();

    // Read all events.
    HepMC3::ReaderAscii reader(newValue);
    while (!reader.failed()) {
      std::unique_ptr<HepMC3::GenEvent> event(new HepMC3::GenEvent);
      if (!reader.read_event(*event) || reader.failed()) {
        // No more event, the unique_ptr will free the memory.
        break;
      }
      fSettings.events.push_back(event.release());
    }

    size_t numEvents = fSettings.events.size();
    if (numEvents == 0) {
      G4Exception("GeneratorSettingsMessenger::SetNewValue", "0001",
                  FatalException, "Could not load HepMC3 file!");
    }

    G4cout << "Read " << fSettings.events.size() << " events from " << newValue
           << G4endl << G4endl;
  }
  // Avoid compiler warnings.
  (void)command;
  (void)newValue;
}
#endif
