//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//

#include "DetectorConstruction.hh"
#include "ActionInitialization.hh"

#ifdef HAVE_HEPMC3
#include "GeneratorSettings.hh"
#endif

#include "G4RunManagerFactory.hh"

#include "G4UImanager.hh"
#include "G4UIcommand.hh"
#include "G4PhysListFactory.hh"

#include "Randomize.hh"

#include "G4VisExecutive.hh"
#include "G4UIExecutive.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

namespace {
  void PrintUsage() {
    G4cerr << " Usage: " << G4endl;
    G4cerr << " calTest [-m macro ] [-hepmc] [-u UIsession]" << G4endl;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main(int argc,char** argv)
{
  // Evaluate arguments
  //
  if ( argc > 8 ) {
    PrintUsage();
    return 1;
  }
  G4String macro;
  G4String session;
  G4bool ifHepMC = false;
  for ( G4int i=1; i<argc; ++i ) {
    if      ( G4String(argv[i]) == "-m" ) {macro = argv[i+1]; ++i;}
    else if      ( G4String(argv[i]) == "-hepmc" ) ifHepMC = true;
    else if ( G4String(argv[i]) == "-u" ) {session = argv[i+1]; ++i;}
    else {
      G4cout << " I do not understand command: " << argv[i] << G4endl;
      PrintUsage();
      return 1;
    }
  }
  // Detect interactive mode (if no macro provided) and define UI session
  //
  G4UIExecutive* ui = nullptr;
  if ( ! macro.size() ) {
    ui = new G4UIExecutive(argc, argv, session);
  }

  // Choose the Random engine
  //
  G4Random::setTheEngine(new CLHEP::RanecuEngine);

  // Construct the default run manager
  //
  auto* runManager =
    G4RunManagerFactory::CreateRunManager(G4RunManagerType::Default);

  // Set mandatory initialization classes
  //
  auto detConstruction = new DetectorConstruction();
  runManager->SetUserInitialization(detConstruction);

  G4PhysListFactory factory;
  auto* physicsList = factory.GetReferencePhysList("FTFP_BERT");
  runManager->SetUserInitialization(physicsList);

  ActionInitialization* actionInitialization;

  GeneratorSettingsMessenger* gsMessenger;
  if(ifHepMC) {
    #ifndef HAVE_HEPMC3
    G4Exception("main()", "HepMC3NotFound",FatalException, "CMake did not find HepMC3, configure it!");
    #else
    gsMessenger = new GeneratorSettingsMessenger();
    actionInitialization = new ActionInitialization(&gsMessenger->GetGeneratorSettings());
    #endif
  } else {
    actionInitialization = new ActionInitialization();
  }
  runManager->SetUserInitialization(actionInitialization);

  // Initialize visualization
  //
  auto visManager = new G4VisExecutive;
  visManager->Initialize();

  // Get the pointer to the User Interface manager
  auto UImanager = G4UImanager::GetUIpointer();

  // Process macro or start UI session
  if ( macro.size() ) {
    // batch mode
    G4String command = "/control/execute ";
    UImanager->ApplyCommand(command+macro);
  }
  else  {
    // interactive mode : define UI session
    UImanager->ApplyCommand("/control/execute init_vis.mac");
    if (ui->IsGUI()) {
      UImanager->ApplyCommand("/control/execute gui.mac");
    }
    ui->SessionStart();
    delete ui;
  }

  // Job termination
  // Free the store: user actions, physics_list and detector_description are
  // owned and deleted by the run manager, so they should not be deleted
  // in the main() program !


  if(ifHepMC) {
    delete gsMessenger;
  }
  delete visManager;
  delete runManager;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo.....
